from setuptools import setup

package_name = 'agv_perception'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='worachet_pond',
    maintainer_email='worachet_pond@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'spi = agv_perception.spiControl:main',
            'lidar = agv_perception.lidarServo:main',
            'tracking = agv_perception.lineTracking:main'
        ],
    },
)
