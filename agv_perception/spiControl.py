import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist,Vector3
from sensor_msgs.msg import Range
from std_msgs.msg import Header
from nav_msgs.msg import Odometry
import numpy as np
import spidev
import zlib

class spiControl(Node):
    def __init__(self):
        super().__init__('spi_control')
        self.publisher_ultrasonic_L = self.create_publisher(Range,'AGV/distanceLeft',10)
        self.publisher_ultrasonic_R = self.create_publisher(Range,'AGV/distanceRight',10)
        self.subscription_twist = self.create_subscription(Twist,'cmd_vel',self.twist_callback,10)
        self.spi_timer = self.create_timer(0.002,self.spi_timer_callback)
        self.spi = spidev.SpiDev(0,0)
        self.spi.max_speed_hz = 1125000
        self.spi_buffer = [0]*20
        self.rpmLeft,self.rpmRight = 0,0
        self.ultrasonic_L,self.ultrasonic_R = Range,Range
        self.ultrasonic_L.min_range,self.ultrasonic_R.min_range = 0.15,0.15
        self.ultrasonic_L.max_range,self.ultrasonic_R.max_range = 3,3
        self.ultrasonic_L.radiation_type,self.ultrasonic_R = 0,0
        self.encoder_L,self.encoder_R = 0,0
        self.WHEEL_DIST = 0.46
        self.WHEEL_Diameter = 6.5 * 0.0254 #6.5 inch to meter

    def spi_timer_callback(self):
        self.spi_buffer[0] = (self.rpmLeft >> 8) & 0xFF
        self.spi_buffer[1] = self.rpmLeft & 0xFF
        self.spi_buffer[2] = (self.rpmRight >> 8) & 0xFF
        self.spi_buffer[3] = self.rpmRight & 0xFF
        crc = zlib.crc32(bytes(self.spi_buffer[0:17]))
        self.spi_buffer[16] = (crc >> 24) & 0xFF
        self.spi_buffer[17] = (crc >> 16) & 0xFF
        self.spi_buffer[18] = (crc >> 8) & 0xFF
        self.spi_buffer[19] = crc & 0xFF
        self.spi.xfer(self.spi_buffer)
        crc = zlib.crc32(bytes(self.spi_buffer[0:16]))
        if crc == int.from_bytes(bytes(self.spi_buffer[16:20])):
            self.encoder_L = int.from_bytes(bytes(self.spi_buffer[0:5]),'big',signed = True)
            self.encoder_R = int.from_bytes(bytes(self.spi_buffer[5:9]),'big',signed = True)
            self.ultrasonic_L = int.from_bytes(bytes(self.spi_buffer[9:11]),'big')
            self.ultrasonic_R = int.from_bytes(bytes(self.spi_buffer[11:14]),'big')
        
    def twist_callback(self,msg):
        angle = msg.angular.z
        speed = msg.linear.x
        self.rpmRight = ((angle*self.WHEEL_DIST)/2 + speed) * (60/(np.pi*self.WHEEL_Diameter))
        self.rpmLeft = (speed*2 *(60/(np.pi*self.WHEEL_Diameter)))-self.rpmRight
        print("LeftSpeed = {},RightSpeed = {}".format(self.rpmLeft,self.rpmRight))

def main(args=None):
    rclpy.init(args=args)
    spi_control = spiControl()
    rclpy.spin(spi_control)
    spi_control.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
