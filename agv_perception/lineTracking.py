import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32

class  LineTracking(Node):
    def __init__(self):
        super().__init__('line_tracking')
        self.baseDrivePublisher = self.create_publisher(Twist,'AGV/cmd_vel',10)
        self.cmd_vel_subscripe = self.create_subscription(Twist,'cmd_vel',self.cmd_vel_callback,10)
        self.heading_subscripe = self.create_subscription(Float32,'AGV/heading',self.heading_callback,10)
        self.control_timer = self.create_timer(0.02,self.control_Timer_callback)
        self.cmd_vel_msg = Twist
        self.heading = Float32
        self.onTracking = False
        self.Kpid = [1,0,0]
        self.proportional = 0
        self.integral = 0
        self.derivative = 0
        self.last_proportional = 0
    def cmd_vel_callback(self,msg):
        self.cmd_vel_msg = msg
    def heading_callback(self,msg):
        self.heading = msg
    def control_Timer_callback(self):
        Outcmd_vel_msg = Twist
        if self.onTracking == True:
            self.proportional = self.heading.data
            self.integral += self.proportional * 0.02
            self.derivative = (self.proportional - self.last_proportional) / 0.02
            P = self.Kpid[0] * self.proportional
            I = self.Kpid[1] * self.integral
            D = self.Kpid[2] * self.derivative
            self.last_proportional = self.proportional
            PID_sum = P+I+D
            Outcmd_vel_msg.linear.x = self.cmd_vel_msg.linear.x
            Outcmd_vel_msg.angular.z = PID_sum

def main(args=None):
    rclpy.init(args=args)
    line_tracking = LineTracking()
    rclpy.spin(line_tracking)
    line_tracking.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()