import rclpy
from rclpy.node import Node
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header
import numpy as np
from smbus2 import SMBus , i2c_msg
import RPi.GPIO as GPIO
from datetime import datetime

class Lidar(Node):
    def __init__(self):
        super().__init__('lidar')
        self.publish_laser = self.create_publisher(LaserScan,'/scan',10)
        self.msg = LaserScan
        lidar_header = Header
        lidar_header.frame_id = "/laser_fram"
        self.msg.header = lidar_header
        self.msg.angle_min = 0
        self.msg.angle_max = np.pi
        self.msg.angle_increment = np.pi/180
        self.msg.range_min = 0.01
        self.msg.range_max = 40.0
        self.msg.scan_time = 0.02
        self.msg.time_increment = 0.02
        self.msg.ranges = []*180
        GPIO.setwarnings(False)			#disable warnings
        GPIO.setmode(GPIO.BOARD)		
        GPIO.setup(33,GPIO.OUT)
        self.servo_pwm = GPIO.PWM(18,50)
        self.servo_pwm.start(0)
        self.angle = np.linspace(2,12.5,180)
        self.angle_Now = 0
        self.is_CW = True
        self.servoWrite(0)
        self.timer = self.create_timer(0.02,self.timer_callback)
    def read_lidar(self):
        with SMBus(1) as bus:
            bus.write_byte_data(0x62,0,0x04)
            #status = bus.read_byte_data(0x62, 0x01)
            M_BIT = bus.read_byte_data(0x62, 0x0f)
            L_BIT = bus.read_byte_data(0x62, 0x10)
            lidar_Distance = (M_BIT << 8)|(L_BIT & 0xff)
            return lidar_Distance/100
    def servoWrite(self,degree):
        self.servo_pwm.ChangeDutyCycle(self.angle[degree-1])
    def timer_callback(self):
        if self.is_CW == True:
            if self.angle_Now < 180:
                self.msg.ranges[self.angle_Now] = self.read_lidar()
                self.angle_Now += 1
            else:
                self.msg.ranges[self.angle_Now] = self.read_lidar()
                self.publish_laser.publish(self.msg)
                self.is_CW = False
        else:
            if self.angle_Now > 0:
                self.msg.ranges[self.angle_Now] = self.read_lidar()
                self.angle_Now -= 1
            else:
                self.msg.ranges[self.angle_Now] = self.read_lidar()
                self.publish_laser.publish(self.msg)
                self.is_CW = True
def main(args=None):
    rclpy.init(args=args)

    lidar = Lidar()

    rclpy.spin(lidar)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    lidar.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()